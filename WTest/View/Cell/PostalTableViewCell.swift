//
//  PostalTableViewCell.swift
//  WTest
//
//  Created by Rodrigo Dias de Souza on 07/03/21.
//

import UIKit

class PostalTableViewCell: UITableViewCell {
  
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  @IBOutlet weak var titleLabel: UILabel! {
    didSet{
      let pointSize = titleLabel.font.pointSize
      titleLabel.font = UIFont.systemFont(ofSize: pointSize, weight: UIFont.Weight.bold)
      titleLabel.textColor = UIColor.black
    }
  }
}
