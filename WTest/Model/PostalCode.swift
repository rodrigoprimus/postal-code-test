//
//  PostalCode.swift
//  WTest
//
//  Created by Rodrigo Dias de Souza on 06/03/21.
//  Copyright © 2021 RodrigoPrimus. All rights reserved.
//

import Foundation

struct PostalCode: Decodable {
  let cod_distrito:String
  let cod_concelho:String
  let cod_localidade:String
  let nome_localidade:String
  let cod_arteria:String
  let tipo_arteria:String
  let prep1:String
  let titulo_arteria:String
  let prep2:String
  let nome_arteria:String
  let local_arteria:String
  let troco:String
  let porta:String
  let cliente:String
  let num_cod_postal:String
  let ext_cod_postal:String
  let desig_postal:String
  
  enum CodingKeys: String, CodingKey {
    case cod_distrito
    case cod_concelho
    case cod_localidade
    case nome_localidade
    case cod_arteria
    case tipo_arteria
    case prep1
    case titulo_arteria
    case prep2
    case nome_arteria
    case local_arteria
    case troco
    case porta
    case cliente
    case num_cod_postal
    case ext_cod_postal
    case desig_postal
  }
}
