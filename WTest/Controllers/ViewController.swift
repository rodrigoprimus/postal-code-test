//
//  ViewController.swift
//  WTest
//
//  Created by Rodrigo Dias de Souza on 07/03/21.
//

import UIKit
import Alamofire
import CoreData

class ViewController: UIViewController {
  
  var postalCodeData:[PostalCode]?
  
  let fileName = "codigos_postais.csv"
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    //TODO: verificar se há CoreData salvo / remover fileExists()
    if let _ = loadCoreData() {
      print("has COREDATA")
      //postalCodeData = coreData
      //tableView.reloadData()
      //TODO: preciso implementar o parse correto dos dados recebidos do CoreData
    } else {
      //downloadFile()
    }
    
    if(fileExists(fileName: fileName)) {
      let paths = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)
      let cachesDirectory: URL = paths[0]
      let cachedFilePath = "\(cachesDirectory.path)/\(fileName)"
      parseCSV(path:cachedFilePath)
    } else {
      downloadFile()
    }
  }
  
  @IBOutlet weak var loadingView: UIActivityIndicatorView!
  
  //TODO: busca não implementada
  @IBOutlet weak var searchView: UIView!
  
  @IBOutlet weak var tableView: UITableView! {
    didSet {
      tableView.dataSource = self
      tableView.rowHeight = UITableView.automaticDimension
      tableView.estimatedRowHeight = 40
      tableView.isHidden = true
      
      tableView.register(
        UINib(nibName: "PostalTableViewCell", bundle: nil),
        forCellReuseIdentifier: "PostalCell")
    }
  }
  
  //Download CSV file to CACHE Directory
  func downloadFile(){
    
    let documentsURL = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)[0]
    let fileURL = documentsURL.appendingPathComponent(fileName)
    
    let destination: DownloadRequest.Destination = { _, _ in
      return (fileURL, [.createIntermediateDirectories])
    }
    
    let urlFile = "https://raw.githubusercontent.com/centraldedados/codigos_postais/master/data/codigos_postais.csv"
    var request = try! URLRequest(url: urlFile, method: .get)
    request.addValue("*/*;q=0.8", forHTTPHeaderField: "Accept")
    
    AF.download(request, to: destination).response { [weak self] response in
      guard let strongSelf = self else { return }
      
      if let url = response.fileURL {
        strongSelf.parseCSV(path:url.path)
      }
      else {
        let string = "Ocorreu um erro ao fazer download do arquivo."
        strongSelf.loadingView.stopAnimating()
        print(string)
      }
    }
  }
  
  //CHECK if file exists on CACHE Directory
  func fileExists(fileName:String) -> Bool {
    let path = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)[0] as String
    let url = NSURL(fileURLWithPath: path)
    if let pathComponent = url.appendingPathComponent(fileName) {
      let filePath = pathComponent.path
      let fileManager = FileManager.default
      if fileManager.fileExists(atPath: filePath) {
        print("FILE AVAILABLE!")
        return true
      } else {
        print("FILE NOT AVAILABLE!")
        return false
      }
    } else {
      print("FILE PATH NOT AVAILABLE!")
      return false
    }
  }
  
  //Parse CSV data to Dictionary
  func parseCSV(path:String?) {
    if let pathURL = path {
      DispatchQueue.main.async { [unowned self] in
        do {
          let csvString = try String (contentsOfFile: pathURL, encoding: String.Encoding.utf8)
          var csvArray = csvString.components (separatedBy: .newlines)
          let columnNames = csvArray[0].components(separatedBy: ",")
          
          //remove column line
          csvArray.remove(at: 0)
          
          postalCodeData = [PostalCode]()
          for item in csvArray {
            let values = item.components(separatedBy: ",")
            //        for x in 0..<5 {
            //          let values = csvArray[x].components(separatedBy: ",")
            
            var params = [String: String]()
            
            let valueCount = values.count
            for i in 0..<columnNames.count {
              let value = valueCount > i ? values[i] : ""
              params[columnNames[i]] = value
            }
            
            let JSON = json(from:params as Any)!
            let jsonData = JSON.data(using: .utf8)!
            let postal: PostalCode = try! JSONDecoder().decode(PostalCode.self, from: jsonData)
            
            postalCodeData?.append(postal)
          }
        } catch let error as NSError {
          print ("ERROR: \(error.localizedDescription)")
        }
        
        //não é necessário verificar "loadCoreData()"
        if (postalCodeData != nil && loadCoreData() == nil) {
          print("...INIT SAVE on CoreData")
          saveCoreData(postalCodeData!)
        }
        
        tableView.isHidden = false
        tableView.reloadData()
        
        loadingView.stopAnimating()
      }
    }
  }
  
  //CoreData save
  func saveCoreData(_ codes: [PostalCode]) {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let context = appDelegate.persistentContainer.viewContext
    for item in codes {
      let newPostalCode = NSEntityDescription.insertNewObject(forEntityName: "PostalCode", into: context)
      newPostalCode.setValue(item.num_cod_postal, forKey: "num_cod_postal")
      newPostalCode.setValue(item.ext_cod_postal, forKey: "ext_cod_postal")
      newPostalCode.setValue(item.desig_postal, forKey: "desig_postal")
    }
    do {
      try context.save()
      print("CoreData saved!")
    } catch {
      print("ERROR saving CoreData: \(error)")
    }
  }
  
  //CoreData retrieve
  func loadCoreData() -> [NSManagedObject]? {
    var data: [NSManagedObject]?
    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
      return nil
    }
    
    let managedContext = appDelegate.persistentContainer.viewContext
    let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "PostalCode")
    
    do {
      data = try managedContext.fetch(fetchRequest)
    } catch let error as NSError {
      print("Could not fetch. \(error), \(error.userInfo)")
    }
    return data
  }
}

//tableview datasource
extension ViewController: UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return postalCodeData?.count ?? 0
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "PostalCell", for: indexPath) as! PostalTableViewCell
    
    if let data = postalCodeData?[indexPath.row] {
      cell.titleLabel.text = "\(data.num_cod_postal)-\(data.ext_cod_postal) \(data.desig_postal)"
    }
    return cell
  }
}

//JSON serializar
func json(from object:Any) -> String? {
  guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
    return nil
  }
  return String(data: data, encoding: String.Encoding.utf8)
}
